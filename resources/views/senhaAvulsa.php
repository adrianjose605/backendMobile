<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Senha</title>
    <!-- <script type="text/javascript">
      self.blur()
    </script> -->
    <style type="text/css" media="print">
    @page
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
    </style>
    <style>
    @media print {
      body * {
        visibility: hidden;
        }
        .print, .print * {
          visibility: visible;
        }
        .print {
          position: absolute;
          left: 0;
          top: 0;
        }
    }
    * {
        background:transparent !important;
        color:#000 !important;
        text-shadow:none !important;
        filter:none !important;
        -ms-filter:none !important;
      }
      body {
        font-family: 'Arial';
        margin:0;
        padding:0;
      }
      .senha {
        margin-left: 50px;
        font-size: 4.3em;
        font-weight: bold;
      }
      .fila {
        font-size: 0.7em;
        margin-top: 17px;
        margin-left: 10px;
      }
      .data {
        font-size: 0.7em;
        margin-top: 10px;
        margin-left: 10px;
      }
    </style>
  </head>
  <body>
    <div class="print">
      <div class="senha">
        <span><?php echo $senha ?></span>
      </div>
      <div class="fila">
        <span><strong>Fila: <?php echo $fila ?></strong></span>
      </div>
      <div class="data">
        <span>Data: <?php echo $dtSenha ?></span>
        <span style="margin-left: 4em">Hora: <?php echo $hrSenha ?></span>
      </div>
    </div>
    <script type="text/javascript">
      window.print()
      location.href = "<?php echo $callbackUrl ?>"
    </script>
  </body>
</html>
