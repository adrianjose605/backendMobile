<?php

namespace App\Classes;

use Illuminate\Http\Response;

class Utils
{
    public static function verificaElegibilidade($dadosBeneficiario)
    {
        $dataSolicitacao = date('Y-m-d');
        $horaSolicitacao = date('H:i:s');
        $carteirinha = $dadosBeneficiario['carteirinha'];
        $nomeBeneficiario = $dadosBeneficiario['nomeBeneficiario'];

        $cliente = array(
          'tipoTransacao' => 'VERIFICA_ELEGIBILIDADE',
          'sequencialTransacao' => '482380',
          'dataRegistroTransacao' => $dataSolicitacao,
          'horaRegistroTransacao' => $horaSolicitacao,
          'codigoPrestadorNaOperadora1' => '10420846000134',
          'registroANS' => '367095',
          'versaoPadrao' => '3.02.01',
          'codigoPrestadorNaOperadora2' => '10420846000134',
          'nomeContratado' => 'SAMEL SERVICOS DE ASSISTENCIA MEDICO HOSPITALAR LTDA',
          'numeroCarteira' => $carteirinha,
          'nomeBeneficiario' => $nomeBeneficiario,
          'numeroCNS' => '2017199',
          'validadeCarteira' => $dataSolicitacao
        );

        $hashTemp = null;
        foreach ($cliente as $value) {
            $hashTemp .= $value;
        }
        $cliente['hash'] = md5($hashTemp);

        $chamada = array(
          'pedidoElegibilidadeWS' => array(
            'cabecalho' => array(
                'identificacaoTransacao' => array(
                    'tipoTransacao' => $cliente['tipoTransacao'],
                    'sequencialTransacao' => $cliente['sequencialTransacao'],
                    'dataRegistroTransacao' => $cliente['dataRegistroTransacao'],
                    'horaRegistroTransacao' => $cliente['horaRegistroTransacao'],
                ),
                'origem' => array(
                    'identificacaoPrestador' => array(
                        'codigoPrestadorNaOperadora' => $cliente['codigoPrestadorNaOperadora1']
                    )
                ),
                'destino' => array(
                    'registroANS' => $cliente['registroANS']
                ),
                'versaoPadrao' => $cliente['versaoPadrao']
            ),
            'pedidoElegibilidade' => array(
              'dadosPrestador' => array(
                'codigoPrestadorNaOperadora' => $cliente['codigoPrestadorNaOperadora2'],
                'nomeContratado' => $cliente['nomeContratado']
              ),
              'numeroCarteira' => $cliente['numeroCarteira'],
              'nomeBeneficiario' => $cliente['nomeBeneficiario'],
              'numeroCNS' => $cliente['numeroCNS'],
              'validadeCarteira' => $cliente['validadeCarteira']
            ),
            'hash' => $cliente['hash']
          )
        );

        $uri = "http://192.168.2.15/ServicesTISS/tissVerificaElegibilidadeV3_02_01.svc?wsdl";
        $function = 'tissVerificaElegibilidade_Operation';
        $client = new \SoapClient($uri);
        $result = $client->__soapCall($function, $chamada);
        $resposta = '';
        if (is_soap_fault($result)) {
            $resposta = $result;
        } else {
            $resposta = array(
              'carteirinha' => $result->respostaElegibilidade->reciboElegibilidade->numeroCarteira,
              'beneficiario' => $result->respostaElegibilidade->reciboElegibilidade->nomeBeneficiario,
              'resposta' => $result->respostaElegibilidade->reciboElegibilidade->respostaSolicitacao
            );
            if ($result->respostaElegibilidade->reciboElegibilidade->respostaSolicitacao == 'N') {
                $resposta['negativa'] = $result->respostaElegibilidade->reciboElegibilidade->motivosNegativa->motivoNegativa->descricaoGlosa;
            }
        }
      return $resposta;
    }

    public static function Response($status, $data, $cors = false, $httpStatusCode = 200){
      $response['status'] = $status;
      foreach ($data as $key => $value) {
        $response[$key] = $value;
      }
      if ($cors) {
        return response()->json($response,$httpStatusCode,['Access-Control-Allow-Origin'=>'*']);
      }else{
        return response()->json($response,$httpStatusCode);
      }
    }
}
