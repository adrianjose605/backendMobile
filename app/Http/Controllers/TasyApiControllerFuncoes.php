<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\DAO\TasyDAO;
use App\DAO\DAOUtils;
use App\Classes\Utils;
use \Firebase\JWT\JWT;
use PDO;

class TasyApiControllerFuncoes extends Controller
{
    public function __construct()
    {
      $this->conn = DAOUtils::getTasyConnection();
    }


  public function dataPerson(Request $request)
  {

    if($request->input('cd_pessoa_fisica')){      
        $stmt = $this->conn->prepare("SELECT * 
      FROM SAMEL.vw_paciente_internado_pesquisa A 
      WHERE A.CD_PESSOA_FISICA = :cd_pessoa_fisica");
        
        $stmt->bindValue(':cd_pessoa_fisica', $request->input('cd_pessoa_fisica'));
        $exec = $stmt->execute();
        $data =  $stmt->fetchAll(PDO::FETCH_ASSOC);        
        return response()->json($data,200);
    }else{
        return response()->json(['err'=>'parameter not valid'],400);
    }

  }





}
