<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\DAO\TasyDAO;
use App\DAO\DAOUtils;
use App\Classes\Utils;
use PDO;
use \Firebase\JWT\JWT;

class AuthController extends Controller
{
    public function __construct()
    {
     // $this->conn = DAOUtils::getTasyConnection();
    }

    public function login(Request $req){
    	if($req->input('id')){
    		return response()->json(['token'=>$this->jwt($req->input('id'))],200);
    	}else
    		return response()->json(['err'=>'parameter not valid'],500);

    	//	return response()->json(['token'=>$this->jwt($req->input('id'))],200);
    	//	return response()->json(['err'=>'id not found',404);

    }

    public function recovery(Request $req){}

  public function test(){
    return phpinfo();
  }
 
    protected function jwt($id){
    	$payload=[
    		'iss'=>'lumen-jwt',
    		'sub'=>$id,
    		'iat'=>time(),
    		'exp'=>time()+60*60
    	];
    	return JWT::encode($payload,env('JWT_SECRET'));
    }



}